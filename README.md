# README #

**1st project MatrixMultiplier** was performed as Insart Java trainee

This app can multiply 2  matrices due to multithreading and then, if you want, it can shows you the history by the date you have chosen.
You can see the result of working this program by seeing  /MatrixMultiply / demo-pic /

**Note:** This application was written for port 8080. And for using this project you should change the login and the password in MatrixMultiply / src / main / resources /config.properties

**2nd project SummaryTask4** was performed as part of EPAM java training courses

This is a tour site, where everyone can buy any tour he wants. In this project were used two databases: MySQL & H2.

**Note:** To run this project you should change db property file in ua.nure.dominov.SummaryTask4.properties.config
and have a suitable database, such as given in the root of the project.

By Nikita Dominov